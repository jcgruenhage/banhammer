use futures::{
    future::{lazy, Future},
    stream::Stream,
};
use std::time::{Duration, Instant};
use tokio::timer::Interval;
use url::Url;

use prometheus::*;

use lazy_static::lazy_static;

mod matrix;
mod metrics;

lazy_static! {
    static ref KICK_COUNTER: Counter = register_counter!(opts!(
        "kicks_total",
        "Total number of kicks performed.",
        labels! {"room" => "all",}
    ))
    .unwrap();
    static ref BAN_COUNTER: Counter = register_counter!(opts!(
        "bans_total",
        "Total number of bans performed.",
        labels! {"room" => "all",}
    ))
    .unwrap();
    static ref KICK_CHECK_COUNTER: Counter = register_counter!(opts!(
        "kick_checks_total",
        "Total number of kick checks performed.",
        labels! {"room" => "all",}
    ))
    .unwrap();
    static ref BAN_CHECK_COUNTER: Counter = register_counter!(opts!(
        "ban_checks_total",
        "Total number of ban checks performed.",
        labels! {"room" => "all",}
    ))
    .unwrap();
}

fn main() {
    let client = matrix::Client {
        token: std::env::var("BANHAMMER_TOKEN").unwrap(),
        url: Url::parse(&std::env::var("BANHAMMER_HOMESERVER").unwrap()).unwrap(),
        client: reqwest::Client::new(),
    };
    tokio::run(lazy(move || {
        //TODO: Start metrics server here
        metrics::start_serving_metrics();
        tokio::spawn(
            Interval::new(Instant::now(), Duration::from_secs(43200))
                .for_each(move |_| {
                    for room in dbg!(client.joined_rooms()) {
                        let policy = dbg!(client.get_banhammer_policy(room.clone()));
                        let members = dbg!(client.joined_members(room.clone()));

                        for regex in policy.kick {
                            for member in members.clone() {
                                KICK_CHECK_COUNTER.inc();
                                if dbg!(regex.is_match(&member)) {
                                    client.kick(
                                        room.clone(),
                                        member,
                                        String::from("Kicked according to banhammer policy"),
                                    );
                                    KICK_COUNTER.inc();
                                }
                            }
                        }

                        for regex in policy.ban {
                            for member in members.clone() {
                                BAN_CHECK_COUNTER.inc();
                                if dbg!(regex.is_match(&member)) {
                                    client.ban(
                                        room.clone(),
                                        member,
                                        String::from("Banned according to banhammer policy"),
                                    );
                                    BAN_COUNTER.inc();
                                }
                            }
                        }
                    }
                    Ok(())
                })
                .map_err(|_| ()),
        );
        Ok(())
    }));
}
