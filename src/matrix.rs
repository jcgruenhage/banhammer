use reqwest;
use url::Url;

use serde::{Deserialize, Serialize};

use std::collections::HashMap;

use regex::Regex;

use lazy_static::lazy_static;

use prometheus::*;

lazy_static! {
    static ref HTTP_COUNTER: Counter = register_counter!(opts!(
        "http_requests_total",
        "Total number of HTTP requests made.",
        labels! {"handler" => "all",}
    ))
    .unwrap();
    static ref HTTP_BODY_GAUGE: Gauge = register_gauge!(opts!(
        "http_response_size_bytes",
        "The HTTP response sizes in bytes.",
        labels! {"handler" => "all",}
    ))
    .unwrap();
    static ref TIME_SPENT_KICKING: HistogramVec = register_histogram_vec!(
        "time_spent_kicking",
        "The time waited for synapse to kick someone in seconds.",
        &["room", "user"]
    )
    .unwrap();
    static ref TIME_SPENT_BANNING: HistogramVec = register_histogram_vec!(
        "time_spent_banning",
        "The time waited for synapse to bann someone in seconds.",
        &["room", "user"]
    )
    .unwrap();
}

pub struct Client {
    pub token: String,
    pub url: Url,
    pub client: reqwest::Client,
}

#[derive(Deserialize)]
struct JoinedRooms {
    joined_rooms: Vec<String>,
}

#[derive(Deserialize)]
struct JoinedMembers {
    joined: HashMap<String, serde_json::Value>,
}

#[derive(Deserialize, Debug)]
pub struct BanhammerPolicyRaw {
    kick: Option<Vec<String>>,
    ban: Option<Vec<String>>,
}

#[derive(Debug)]
pub struct BanhammerPolicy {
    pub(crate) kick: Vec<Regex>,
    pub(crate) ban: Vec<Regex>,
}

#[derive(Serialize)]
struct KickBanTarget {
    reason: String,
    user_id: String,
}

impl Client {
    pub fn joined_rooms(&self) -> Vec<String> {
        let mut response = self
            .client
            .get(&format!(
                "{base_url}_matrix/client/r0/joined_rooms",
                base_url = self.url
            ))
            .query(&[("access_token", &self.token)])
            .send()
            .unwrap();
        let rooms: JoinedRooms = response.json().unwrap();
        HTTP_COUNTER.inc();
        rooms.joined_rooms
    }

    pub fn joined_members(&self, room_id: String) -> Vec<String> {
        let mut response = self
            .client
            .get(&format!(
                "{base_url}_matrix/client/r0/rooms/{room_id}/joined_members",
                base_url = self.url,
                room_id = room_id
            ))
            .query(&[("access_token", &self.token)])
            .send()
            .unwrap();
        let members: JoinedMembers = response.json().unwrap();
        HTTP_COUNTER.inc();
        let mut result = Vec::new();
        for (user, _) in members.joined {
            result.push(user);
        }
        result
    }

    pub fn get_banhammer_policy(&self, room_id: String) -> BanhammerPolicy {
        let mut response = self
            .client
            .get(&format!(
                "{base_url}_matrix/client/r0/rooms/{room_id}/state/re.jcg.banhammer.targets",
                base_url = self.url,
                room_id = room_id
            ))
            .query(&[("access_token", &self.token)])
            .send()
            .unwrap();
        HTTP_COUNTER.inc();
        let policy: BanhammerPolicyRaw = response.json().unwrap();
        BanhammerPolicy {
            kick: match policy.kick {
                Some(string_kicks) => {
                    let mut kicks = Vec::new();
                    for kick in string_kicks {
                        kicks.push(match Regex::new(&kick) {
                            Ok(regex) => regex,
                            Err(_) => continue,
                        });
                    }
                    kicks
                }
                None => vec![],
            },
            ban: match policy.ban {
                Some(string_bans) => {
                    let mut bans = Vec::new();
                    for ban in string_bans {
                        bans.push(match Regex::new(&ban) {
                            Ok(regex) => regex,
                            Err(_) => continue,
                        });
                    }
                    bans
                }
                None => vec![],
            },
        }
    }

    pub fn kick(&self, room_id: String, user_id: String, reason: String) {
        let timer = TIME_SPENT_KICKING
            .with_label_values(&[&room_id, &user_id])
            .start_timer();
        let response = self
            .client
            .post(&format!(
                "{base_url}_matrix/client/r0/rooms/{room_id}/kick",
                base_url = self.url,
                room_id = room_id,
            ))
            .json(&KickBanTarget { reason, user_id })
            .query(&[("access_token", &self.token)])
            .send()
            .unwrap();
        timer.observe_duration();
        HTTP_COUNTER.inc();
        dbg!(response);
    }

    pub fn ban(&self, room_id: String, user_id: String, reason: String) {
        let timer = TIME_SPENT_KICKING
            .with_label_values(&[&room_id, &user_id])
            .start_timer();
        let response = self
            .client
            .post(&format!(
                "{base_url}_matrix/client/r0/rooms/{room_id}/ban",
                base_url = self.url,
                room_id = room_id,
            ))
            .json(&KickBanTarget { reason, user_id })
            .query(&[("access_token", &self.token)])
            .send()
            .unwrap();
        timer.observe_duration();
        HTTP_COUNTER.inc();
        dbg!(response);
    }
}
