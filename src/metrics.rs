use hyper::{header::CONTENT_TYPE, service::service_fn_ok, Body, Response, Server};

use prometheus::Encoder;
use prometheus::TextEncoder;

use futures::future::Future;

pub(crate) fn start_serving_metrics() {
    let serve_metrics = || {
        service_fn_ok(|_req| {
            let metric_families = prometheus::gather();
            let mut buffer = vec![];
            let encoder = TextEncoder::new();
            encoder.encode(&metric_families, &mut buffer).unwrap();
            let mut res = Response::new(Body::from(buffer));
            res.headers_mut()
                .insert(CONTENT_TYPE, encoder.format_type().parse().unwrap());
            res
        })
    };
    println!("Listening on [::]:8996");
    let server = Server::bind(&"[::]:8996".parse().unwrap())
        .serve(serve_metrics)
        .map_err(|err| eprintln!("server error: {}", err));
    tokio::spawn(server);
}
