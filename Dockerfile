FROM docker.io/alpine 
RUN apk add --no-cache ca-certificates
ADD target/x86_64-unknown-linux-musl/release/banhammer /usr/bin/banhammer
ENTRYPOINT ["/usr/bin/banhammer"]
